﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

[Serializable]
public class ParentsChildrenSelect
{
    private ArrayList persons;

    private ArrayList parentschildren;

    public ParentsChildrenSelect(ArrayList personsList, ArrayList parentschildrenList)
    { 
        //fill person's list and delete duplicate

        persons = DistinctPersons(personsList);

        //fill parentschildren's list
        parentschildren = new ArrayList();
        foreach (ParentsChildren pc in parentschildrenList)
            parentschildren.Add(pc);
    }


    public ArrayList GetParentsChildrenList()
    {
        return parentschildren;
    }
    
    public ArrayList GetPersonList()
    {
        return persons;
    }


    public ArrayList DistinctPersons(ArrayList persons)
    {
        ArrayList distinctpersons = new ArrayList();
        int i = 0;
        int j = 0;
        foreach (Person pr in persons)
        {
            foreach (Person prnext in persons)
            {
                if (i != j && !(pr.FirstName.Equals(prnext.FirstName) && pr.LastName.Equals(prnext.LastName)))
                    distinctpersons.Add(pr);
            }
        }
        return distinctpersons;
    }

}