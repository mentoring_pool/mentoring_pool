﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

public class DataAccess
{
    public static SqlDataReader GetChildrenByParent(string ParentFN, string ParentLN) 
    { 
        SqlConnection connection = Connection.GetDBConnection(); 

        try
        {
            SqlCommand command = new SqlCommand("spGetChildrenByParent", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ParentFN", ParentFN);
            command.Parameters.AddWithValue("@ParentLN", ParentLN);
            SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleResult | CommandBehavior.CloseConnection);
            
            return reader;
        }
        catch (SqlException ex)
        {
            throw;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }

    }


    public static SqlDataReader GetParentsByChild(string ChildFN, string ChildLN)
    {
        SqlConnection connection = Connection.GetDBConnection();

        try
        {
            SqlCommand command = new SqlCommand("spGetParentsByChild", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ChildFN", ChildFN);
            command.Parameters.AddWithValue("@ChildLN", ChildLN);
            SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleResult | CommandBehavior.CloseConnection);

            return reader;
        }
        catch (SqlException ex)
        {
            throw;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }

    }


    public static SqlDataReader GetChild(string ChildFN, string ChildLN)
    {
        SqlConnection connection = Connection.GetDBConnection();

        try
        {
            SqlCommand command = new SqlCommand("spGetChild", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ChildFN", ChildFN);
            command.Parameters.AddWithValue("@ChildLN", ChildLN);
            SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleResult | CommandBehavior.CloseConnection);

            return reader;
        }
        catch (SqlException ex)
        {
            throw;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }


    public static SqlDataReader GetParentByFNLN(string ParentFN, string ParentLN)
    {
        SqlConnection connection = Connection.GetDBConnection();

        try
        {
            SqlCommand command = new SqlCommand("spGetParentByFNLN", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ParentFN", ParentFN);
            command.Parameters.AddWithValue("@ParentLN", ParentLN);
            SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleResult | CommandBehavior.CloseConnection);

            return reader;
        }
        catch (SqlException ex)
        {
            throw;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }

    public static SqlDataReader GetAllChildrenParents(SqlConnection connection)
    {

        try
        {
            SqlCommand command = new SqlCommand("spGetAllChildrenParents", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }
        catch (SqlException ex)
        {
            throw;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }

    public static SqlDataReader GetAllParents()
    {
        SqlConnection connection = Connection.GetDBConnection();

        try
        {
            SqlCommand command = new SqlCommand("spGetAllParents", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleResult | CommandBehavior.CloseConnection);
            
            if (reader.Depth == 0) {
                EmptyTableException ex = new EmptyTableException("Exception on page GetParentsChildrens. ");
                throw ex;
            }

            return reader;
        }
        catch (SqlException ex)
        {
            throw ex;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }

    public static SqlDataReader GetAllChildren()
    {
        SqlConnection connection = Connection.GetDBConnection();
        try
        {
            SqlCommand command = new SqlCommand("spGetAllChildren", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleResult | CommandBehavior.CloseConnection);

            return reader;
        }
        catch (SqlException ex)
        {
            throw;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }


    public static void UpdateParent(string ParentFN, string ParentLN, string newParentFN, string newParentLN, string newParentDoB)
    {
        SqlConnection connection = Connection.GetDBConnection();

        try
        {
            SqlCommand command = new SqlCommand("spUpdateParent", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ParentFN", ParentFN);
            command.Parameters.AddWithValue("@ParentLN", ParentLN);
            command.Parameters.AddWithValue("@newParentFN", newParentFN);
            command.Parameters.AddWithValue("@newParentLN", newParentLN);
            command.Parameters.AddWithValue("@newParentDoB", newParentDoB);
            command.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            throw;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }

    }


    public static void SetParentsChildrenPersons(string ParentFN, string ParentLN, string ParentDoB, string ChildFN, string ChildLN, string ChildDoB)
    {
        SqlConnection connection = Connection.GetDBConnection();

        try
        {
            SqlCommand command = new SqlCommand("spSetParentsChildrenPersons", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ParentFN", ParentFN);
            command.Parameters.AddWithValue("@ParentLN", ParentLN);
            command.Parameters.AddWithValue("@ParentDoB", ParentDoB);
            command.Parameters.AddWithValue("@ChildFN", ChildFN);
            command.Parameters.AddWithValue("@ChildLN", ChildLN);
            command.Parameters.AddWithValue("@ChildDoB", ChildDoB);
            command.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            throw;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }

}