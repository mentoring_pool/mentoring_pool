﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditDataBeforSave.aspx.cs" Inherits="EditDataBeforSave" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Table ID="ParentsChildrenTable" runat="server">
        <asp:TableRow>
            <asp:TableCell>Child First Name</asp:TableCell>
            <asp:TableCell>Child Last Name</asp:TableCell>
            <asp:TableCell>Child Birthday</asp:TableCell>
            <asp:TableCell>Parent First Name</asp:TableCell>
            <asp:TableCell>Parent Last Name</asp:TableCell>
            <asp:TableCell>Parent Birthday</asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Button ID="btnSaveToDatabase" runat="server" Text="Save" OnClick="btnSaveToDatabase_Click" />
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
</asp:Content>
