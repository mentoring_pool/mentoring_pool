﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public partial class EditDataBeforSave : System.Web.UI.Page
{
    ArrayList parentschildren = new ArrayList();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //ParentsChildrenSelect parentschildrenselect = (ParentsChildrenSelect)this.Session["ParentsChildrenObject"];
        
        IFormatter formatterDeSerialize = new BinaryFormatter();
        try
        {
            Stream streamDeSerialize = new FileStream(Server.MapPath("ParentsChildrenFile.bin/ParentsChildrenFile.xml"), FileMode.Open, FileAccess.Read, FileShare.Read);
            ParentsChildrenSelect parentschildrenselect = (ParentsChildrenSelect)formatterDeSerialize.Deserialize(streamDeSerialize);
            streamDeSerialize.Close();

            parentschildren = parentschildrenselect.GetParentsChildrenList();
            WriteRows();
        }
        catch (SerializationException ex)
        {
            Label1.Text = ExceptionHelper.GetSerializationExplanation(ex);
        }
        catch (IOException ex)
        {
            Label1.Text = ExceptionHelper.GetFileReadExplanation(ex);
        }
    }

    private void WriteRows()
    {
        TableRow trow;
        TableCell tcellParentFN, tcellParentLN, tcellParentDoB, tcellChildFN, tcellChildLN, tcellChildDoB, bcellDelete;

        int i = 0;
        foreach (ParentsChildren pc in parentschildren)
        {
            trow = new TableRow();
            tcellChildFN = new TableCell();
            tcellChildFN.Controls.Add(RenderTextBox("ChildFN" + i.ToString(), pc.GetChildFN()));
            tcellChildLN = new TableCell();
            tcellChildLN.Controls.Add(RenderTextBox("ChildLN" + i.ToString(), pc.GetChildLN()));
            tcellChildDoB = new TableCell();
            tcellChildDoB.Controls.Add(RenderTextBox("ChildDoB" + i.ToString(), pc.GetChildDoB()));
            tcellParentFN = new TableCell();
            tcellParentFN.Controls.Add(RenderTextBox("ParentFN" + i.ToString(), pc.GetParentFN()));
            tcellParentLN = new TableCell();
            tcellParentLN.Controls.Add(RenderTextBox("ParentLN" + i.ToString(), pc.GetParentLN()));
            tcellParentDoB = new TableCell();
            tcellParentDoB.Controls.Add(RenderTextBox("ParentDoB" + i.ToString(), pc.GetParentDoB()));
            bcellDelete = new TableCell();
            bcellDelete.Controls.Add(RenderButton("Delete" + i.ToString()));
            trow.Cells.Add(tcellChildFN);
            trow.Cells.Add(tcellChildLN);
            trow.Cells.Add(tcellChildDoB);
            trow.Cells.Add(tcellParentFN);
            trow.Cells.Add(tcellParentLN);
            trow.Cells.Add(tcellParentDoB);
            trow.Cells.Add(bcellDelete);
            ParentsChildrenTable.Rows.Add(trow);
            i++;
        }
    }

    
    private TextBox RenderTextBox(string suffix, string text) 
    {
        TextBox tb = new TextBox(); 
        tb.ID = "txt" + suffix;
        tb.Text = text;
        tb.Width = 100;
        tb.TextChanged += new EventHandler(textBox_TextChanged);
        return tb; 
    }

    private Button RenderButton(string suffix)
    {
        Button bt = new Button();
        bt.ID = "btn" + suffix;
        bt.Height = 20;
        bt.Width = 70;
        bt.Text = "Delete";
        bt.Click += new EventHandler(btnDelete_Click);
        return bt;
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        string btIndex = ((Button)sender).ID;
        string parentchildIndex = btIndex.Substring(9);

        parentschildren.RemoveAt(Convert.ToInt32(parentchildIndex));

        RedrawTable();
    }

    protected void textBox_TextChanged(object sender, EventArgs e)
    {
        int i = 0;

        string tbIndex = ((TextBox)sender).ID;
        string tbText = ((TextBox)sender).Text;
        string ChildFN, 
               ChildLN, 
               ChildDoB, 
               ParentFN, 
               ParentLN, 
               ParentDoB;

        foreach (ParentsChildren pc in parentschildren)
        {
            ChildFN = "txtChildFN" + i.ToString();
            ChildLN = "txtChildLN" + i.ToString();
            ChildDoB = "txtChildDoB" + i.ToString();
            ParentFN = "txtParentFN" + i.ToString();
            ParentLN = "txtParentLN" + i.ToString();
            ParentDoB = "txtParentDoB" + i.ToString();

            if (tbIndex.Equals(ChildFN)) pc.GetChild().FirstName = tbText;
            else if (tbIndex.Equals(ChildLN)) pc.GetChild().LastName = tbText;
            else if (tbIndex.Equals(ChildDoB)) pc.GetChild().DateOfBirth = tbText;
            else if (tbIndex.Equals(ParentFN)) pc.GetParent().FirstName = tbText;
            else if (tbIndex.Equals(ParentLN)) pc.GetParent().LastName = tbText;
            else if (tbIndex.Equals(ParentDoB)) pc.GetParent().DateOfBirth = tbText;
            i++;
        }

        RedrawTable(); 
                      
    }

    private void RedrawTable()
    {

        int countControls = ParentsChildrenTable.Controls.Count;
        for (int i = countControls-1; i >= 0 ; i--)
            ParentsChildrenTable.Controls.RemoveAt(i);
        
        WriteRows();
    }



    protected void btnSaveToDatabase_Click(object sender, EventArgs e)
    {
        foreach (ParentsChildren pc in parentschildren)
        {

            try
            {
                DataAccess.SetParentsChildrenPersons(pc.GetParent().FirstName, pc.GetParent().LastName, pc.GetParent().DateOfBirth, pc.GetChild().FirstName, pc.GetChild().LastName, pc.GetChild().DateOfBirth);
            }
            catch (SqlException ex)
            {
                Label1.Text = SqlExceptionHelper.GetErrorExplanation(ex);
            }
        }


    }

}