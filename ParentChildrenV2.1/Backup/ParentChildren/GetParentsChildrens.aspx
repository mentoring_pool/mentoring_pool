﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GetParentsChildrens.aspx.cs" Inherits="GetParentsChildrens" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Show Parents and Children</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:DropDownList ID="ddlParentFirstName" runat="server"></asp:DropDownList>
    <asp:DropDownList ID="ddlParentLastName" runat="server"></asp:DropDownList>
    <asp:Button ID="btnSelectChildrenByParent" runat="server" Text="Select Children" OnClick="btnSelectChildrenByParent_Click" />
    <br/>
    <asp:DropDownList ID="ddlChildFirstName" runat="server"></asp:DropDownList>
    <asp:DropDownList ID="ddlChildLastName" runat="server"></asp:DropDownList>
    <asp:Button ID="btnSelectParentsByChild" runat="server" Text="Select Parents" OnClick="btnSelectParentsByChild_Click" />
    <h2>Parents:</h2>
        <a>Parent's count</a>
        <asp:Label ID="lblParentsCount" runat="server" Text=""></asp:Label>
        <asp:Table ID="ParentsChildren" runat="server">
            <asp:TableRow>
                <asp:TableCell>Parent First Name</asp:TableCell>
                <asp:TableCell>Parent Last Name</asp:TableCell>
                <asp:TableCell>Parent Birthday</asp:TableCell>
            </asp:TableRow>
        </asp:Table>

        <br />
        <asp:Label ID="lblMsg" runat="server" EnableViewState="False"></asp:Label><br />
        <p />

    <h2>Children:</h2>
        <a>Children's count</a>
        <asp:Label ID="lblChildrenCount" runat="server" Text=""></asp:Label>
        <asp:Table ID="Children" runat="server">
            <asp:TableRow>
                <asp:TableCell>Child First Name</asp:TableCell>
                <asp:TableCell>Child Last Name</asp:TableCell>
                <asp:TableCell>Child Birthday</asp:TableCell>
            </asp:TableRow>
        </asp:Table>

        <br/>
        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <br/>
        <a href="Default.aspx">Go Back To Home Page</a>

</asp:Content>
