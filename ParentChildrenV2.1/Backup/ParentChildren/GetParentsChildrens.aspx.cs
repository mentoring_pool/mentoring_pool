﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class GetParentsChildrens : System.Web.UI.Page
{
    public DataSet ds = new DataSet();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) 
        {
            TableRow trow;
            TableCell tcellParentFN, tcellParentLN, tcellParentDoB, tcellChildFN, tcellChildLN, tcellChildDoB, bcellDelete, bcellUpdate;

            try {
                //get all Parents
                using (SqlDataReader readerP = DataAccess.GetAllParents()) 
                {
                    DataTable dtParents = new DataTable();
                    while (!readerP.IsClosed)
                    {
                       dtParents.Load(readerP);
                       lblParentsCount.Text = dtParents.Rows.Count.ToString();
                       
                       ddlParentFirstName.DataSource = dtParents;
                       ddlParentFirstName.DataValueField = "ParentFN";
                       ddlParentFirstName.DataTextField = "ParentFN";
                       ddlParentFirstName.DataBind();
                       ddlParentLastName.DataSource = dtParents;
                       ddlParentLastName.DataValueField = "ParentLN";
                       ddlParentLastName.DataTextField = "ParentLN";
                       ddlParentLastName.DataBind();

                  
                       int i = 0;
                       foreach (DataRow dtRow in dtParents.Rows)
                       {
                           trow = new TableRow();
                           tcellParentFN = new TableCell();
                           tcellParentFN.Controls.Add(RenderTextBox("ParentFN" + i.ToString(), dtRow["ParentFN"].ToString() ));
                           tcellParentLN = new TableCell();
                           tcellParentLN.Controls.Add(RenderTextBox("ParentLN" + i.ToString(), dtRow["ParentLN"].ToString() ));
                           tcellParentDoB = new TableCell();
                           tcellParentDoB.Controls.Add(RenderTextBox("ParentDoB" + i.ToString(), dtRow["ParentDoB"].ToString() ));
                           bcellUpdate = new TableCell();
                           bcellUpdate.Controls.Add(RenderButton("UpdateParent" + i.ToString(), "Update" )); 
                           bcellDelete = new TableCell();
                           bcellDelete.Controls.Add(RenderButton("DeleteParent" + i.ToString(), "Delete" ));
                           trow.Cells.Add(tcellParentFN);
                           trow.Cells.Add(tcellParentLN);
                           trow.Cells.Add(tcellParentDoB);
                           trow.Cells.Add(bcellUpdate);
                           trow.Cells.Add(bcellDelete);
                           ParentsChildren.Rows.Add(trow);
                           i++;
                       }
                    }
                    ds.Tables.Add(dtParents);
                    ds.Tables[0].TableName = "Parents";
                }
                //get all Children
                using (SqlDataReader readerC = DataAccess.GetAllChildren())
                {
                    DataTable dtChildren = new DataTable();
                    while (!readerC.IsClosed)
                    {
                        dtChildren.Load(readerC);
                        ddlChildFirstName.DataSource = dtChildren;
                        ddlChildFirstName.DataValueField = "ChildFN";
                        ddlChildFirstName.DataTextField = "ChildFN";
                        ddlChildFirstName.DataBind();
                        ddlChildLastName.DataSource = dtChildren;
                        ddlChildLastName.DataValueField = "ChildLN";
                        ddlChildLastName.DataTextField = "ChildLN";
                        ddlChildLastName.DataBind();

                        int i = 0;
                        foreach (DataRow dtRow in dtChildren.Rows)
                        {
                            trow = new TableRow();
                            tcellChildFN = new TableCell();
                            tcellChildFN.Controls.Add(RenderTextBox("ChildFN" + i.ToString(), dtRow["ChildFN"].ToString()));
                            tcellChildLN = new TableCell();
                            tcellChildLN.Controls.Add(RenderTextBox("ChildLN" + i.ToString(), dtRow["ChildLN"].ToString()));
                            tcellChildDoB = new TableCell();
                            tcellChildDoB.Controls.Add(RenderTextBox("ChildDoB" + i.ToString(), dtRow["ChildDoB"].ToString()));
                            bcellUpdate = new TableCell();
                            bcellUpdate.Controls.Add(RenderButton("UpdateChild" + i.ToString(), "Update"));
                            bcellDelete = new TableCell();
                            bcellDelete.Controls.Add(RenderButton("DeleteChild" + i.ToString(), "Delete"));
                            trow.Cells.Add(tcellChildFN);
                            trow.Cells.Add(tcellChildLN);
                            trow.Cells.Add(tcellChildDoB);
                            trow.Cells.Add(bcellUpdate);
                            trow.Cells.Add(bcellDelete);
                            Children.Rows.Add(trow);
                            i++;
                        }
                    }
                    ds.Tables.Add(dtChildren);
                    ds.Tables[1].TableName = "Children";
                }
            } catch (SqlException ex) {
                Label1.Text = SqlExceptionHelper.GetErrorExplanation(ex);
            } catch (EmptyTableException ex) {
                lblParentsCount.Text = "0";
                lblChildrenCount.Text = "0";
                Label1.Text = ex.Message() + ex.Text();
                Label1.ForeColor = System.Drawing.Color.Red;
            }
        }
    }

    private TextBox RenderTextBox(string suffix, string text)
    {
        TextBox tb = new TextBox();
        tb.ID = "txt" + suffix;
        tb.Text = text;
        tb.Width = 150;
        //tb.TextChanged += new EventHandler(textBox_TextChanged);
        return tb;
    }

    private Button RenderButton(string suffix, string text)
    {
        Button bt = new Button();
        bt.ID = "btn" + suffix;
        bt.Height = 20;
        bt.Width = 70;
        bt.Text = text;

        if (text.Equals("Delete"))
            bt.Click += new EventHandler(btnDeleteParent_Click);
        else if(text.Equals("Update"))
            bt.Click += new EventHandler(btnUpdateParent_Click);

        return bt;
    }
    
    protected void btnSelectChildrenByParent_Click(object sender, EventArgs e)
    {
        try {
            //get Parent by FirstName and LastName
            using (SqlDataReader readerP = DataAccess.GetParentByFNLN(ddlParentFirstName.SelectedValue, ddlParentLastName.SelectedValue)) 
            {
                DataTable dtParent = new DataTable();
                while (!readerP.IsClosed)
                {
                    dtParent.Load(readerP);
                }
                ds.Tables.Add(dtParent);
            }

            //get Children for specified Parent
            using (SqlDataReader readerC = DataAccess.GetChildrenByParent(ddlParentFirstName.SelectedValue,ddlParentLastName.SelectedValue)) 
            {
                DataTable dtChildren = new DataTable();
                while (!readerC.IsClosed)
                {
                    dtChildren.Load(readerC);
                }
                ds.Tables.Add(dtChildren);
            }
        } catch (SqlException ex) {
            Label1.Text = SqlExceptionHelper.GetErrorExplanation(ex);
        }
    }


    protected void btnSelectParentsByChild_Click(object sender, EventArgs e)
    {
        try
        {
            //get Parent by FirstName and LastName
            using (SqlDataReader readerP = DataAccess.GetParentsByChild(ddlChildFirstName.SelectedValue, ddlChildLastName.SelectedValue))
            {
                DataTable dtParent = new DataTable();
                while (!readerP.IsClosed)
                {
                    dtParent.Load(readerP);
                }
                ds.Tables.Add(dtParent);
            }

            //get specified Child
            using (SqlDataReader readerC = DataAccess.GetChild(ddlChildFirstName.SelectedValue, ddlChildLastName.SelectedValue))
            {
                DataTable dtChildren = new DataTable();
                while (!readerC.IsClosed)
                {
                    dtChildren.Load(readerC);
                }
                ds.Tables.Add(dtChildren);
            }
        }
        catch (SqlException ex)
        {
            Label1.Text = SqlExceptionHelper.GetErrorExplanation(ex);
        }
    }

    protected void btnDeleteParent_Click(object sender, EventArgs e)
    {

    }

    protected void btnAddParent_Click(object sender, EventArgs e)
    {

    }

    protected void btnUpdateParent_Click(object sender, EventArgs e)
    {
        string btIndex = ((TextBox)sender).ID;
        string newParentFN, newParentLN, newParentDoB, oldParentFN, oldParentLN, oldParentDoB;

        string parentIndex = btIndex.Substring(14);
        TextBox tbParentFN = (TextBox)ParentsChildren.FindControl("txtParentFN" + parentIndex);
        TextBox tbParentLN = (TextBox)ParentsChildren.FindControl("txtParentLN" + parentIndex);
        TextBox tbParentDoB = (TextBox)ParentsChildren.FindControl("txtParentDoB" + parentIndex);
        newParentFN = tbParentFN.Text;
        newParentLN = tbParentLN.Text;
        newParentDoB = tbParentDoB.Text;

        oldParentFN = ds.Tables["Parents"].Rows[Convert.ToInt32(parentIndex)].Field<string>("ParentFN");
        oldParentLN = ds.Tables["Parents"].Rows[Convert.ToInt32(parentIndex)].Field<string>("ParentLN");
        //oldParentDoB = ds.Tables["Parents"].Rows[Convert.ToInt32(parentIndex)].Field<string>("ParentDoB");

        try
        {
            //update Parent
            DataAccess.UpdateParent(oldParentFN, oldParentLN, newParentFN, newParentLN, newParentDoB);
        }
        catch (SqlException ex)
        {
            Label1.Text = SqlExceptionHelper.GetErrorExplanation(ex);
        }
        
    }

    protected void btnEditChild_Click(object sender, EventArgs e)
    {

    }

    protected void btnAddChild_Click(object sender, EventArgs e)
    {

    }
}