﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[Serializable]
public class ParentsChildren
{
    private Person Parent;
    private Person Child;

    public ParentsChildren(Person parent, Person child)
    {
        Parent = new Person(parent.FirstName, parent.LastName, parent.DateOfBirth);
        Child = new Person(child.FirstName,child.LastName,child.DateOfBirth);
    }

    public Person GetParent()
    {
        return Parent;
    }

    public Person GetChild()
    {
        return Child;
    }

    public string GetParentFN()
    {
        return Parent.FirstName;
    }

    public string GetParentLN()
    {
        return Parent.LastName;
    }

    public string GetParentDoB()
    {
        return Parent.DateOfBirth;
    }

    public string GetChildFN()
    {
        return Child.FirstName;
    }

    public string GetChildLN()
    {
        return Child.LastName;
    }

    public string GetChildDoB()
    {
        return Child.DateOfBirth;
    }

}