﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[Serializable]
public class Person
{
    private string _FirstName;
    private string _LastName;
    private string _DateOfBirth;

    public string FirstName 
    {
        get 
        {
            return _FirstName;
        }
        set 
        {
            _FirstName = value;
        }
    }
    
    public string LastName
    {
        get 
        {
            return _LastName;
        }
        set 
        {
            _LastName = value;
        }
    }

    public string DateOfBirth
    {
        get
        {
            return _DateOfBirth;
        }
        set
        {
            _DateOfBirth = value;
        }
    }

    public Person(string FirstName, string LastName, string DateOfBirth)
    {
        _FirstName = FirstName;
        _LastName = LastName;
        _DateOfBirth = DateOfBirth;
    }
}