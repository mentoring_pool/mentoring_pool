﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

public class Connection
{
        public static SqlConnection GetDBConnection() 
        {

                string connectionString = ConfigurationManager.ConnectionStrings["ParentChildConnectionString"].ConnectionString;
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                return connection;
         }
}