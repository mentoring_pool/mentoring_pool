﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class GetParentsChildrens : System.Web.UI.Page
{
    public DataSet ds = new DataSet();
    TableRow trow;
    TableCell tcellParentFN, tcellParentLN, tcellParentDoB, tcellChildFN, tcellChildLN, tcellChildDoB, bcellDelete, bcellUpdate;
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack) 
        //{
            try {
                //get all Parents
                using (DataSet dSetP = DataAccess.GetAllParents()) 
                {
                    DataTable dtParents = new DataTable();
                    for (int t = 0; t < dSetP.Tables.Count; t++)
                    {
                        dtParents = dSetP.Tables[t];
                        if (!IsPostBack)
                        {
                            for (int q = 0; q < dtParents.Rows.Count; q++)
                            {
                                string item1 = dtParents.Rows[q]["ParentFN"].ToString();
                                string item2 = dtParents.Rows[q]["ParentLN"].ToString();
                                string appenItems = item1 + " " + item2;
                                ddlParentFullName.Items.Add(new ListItem(appenItems, appenItems));
                            }
                        }
                        ds.Tables.Add(dtParents.Copy());
                        ds.Tables[0].TableName = "Parents";
                        lblParentsCount.Text = ds.Tables[0].Rows.Count.ToString();
                        DrawParentsTable();
                    }
                }
                //get all Children
                using (DataSet dSetC = DataAccess.GetAllChildren())
                {
                    DataTable dtChildren = new DataTable();
                    for (int t = 0; t < dSetC.Tables.Count; t++)
                    {
                        dtChildren = dSetC.Tables[t];
                        if (!IsPostBack)
                        {
                            for (int q = 0; q < dtChildren.Rows.Count; q++)
                            {
                                string item1 = dtChildren.Rows[q]["ChildFN"].ToString();
                                string item2 = dtChildren.Rows[q]["ChildLN"].ToString();
                                string appenItems = item1 + " " + item2;
                                ddlChildFullName.Items.Add(new ListItem(appenItems, appenItems));
                            }
                        }
                        ds.Tables.Add(dtChildren.Copy());
                        ds.Tables[1].TableName = "Children";
                        lblChildrenCount.Text = ds.Tables[1].Rows.Count.ToString();
                        DrawChildrenTable();
                    }
                }
            }
            catch (SqlException ex)
            {
                Label1.Text = SqlExceptionHelper.GetErrorExplanation(ex);
            }
            catch (EmptyTableException ex)
            {
                lblParentsCount.Text = "0";
                lblChildrenCount.Text = "0";
                Label1.Text = ex.Message() + ex.Text();
                Label1.ForeColor = System.Drawing.Color.Red;
            }
        //}
        //else
        //{

        //}
    }

    protected void btnSelectChildrenByParent_Click(object sender, EventArgs e)
    {
        try
        {
            ParentsChildren.Rows.Clear();
            Children.Rows.Clear();
            ds.Reset();
            //get Parent by FirstName and LastName
            using (DataSet dSetP = DataAccess.GetParentByFNLN(ddlParentFullName.SelectedValue.Split()[0], ddlParentFullName.SelectedValue.Split()[1]))
            {
                DataTable dtParent = new DataTable();
                for (int t = 0; t < dSetP.Tables.Count; t++)
                {
                    dtParent = dSetP.Tables[t];
                    dtParent.TableName = "Parents";
                    ds.Tables.Add(dtParent.Copy());
                }
                lblParentsCount.Text = ds.Tables[0].Rows.Count.ToString();
                RedrawParentsTable();


            }

            //get Children for specified Parent
            using (DataSet dSetC = DataAccess.GetChildrenByParent(ddlParentFullName.SelectedValue.Split()[0], ddlParentFullName.SelectedValue.Split()[1]))
            {
                DataTable dtChildren = new DataTable();
                for (int t = 0; t < dSetC.Tables.Count; t++)
                {
                    dtChildren = dSetC.Tables[t];
                    dtChildren.TableName = "Children";
                    ds.Tables.Add(dtChildren.Copy());
                }
                lblChildrenCount.Text = ds.Tables[1].Rows.Count.ToString();
                RedrawChildrenTable();
            }
        }
        catch (SqlException ex)
        {
            Label1.Text = SqlExceptionHelper.GetErrorExplanation(ex);
        }
    }


    protected void btnSelectParentsByChild_Click(object sender, EventArgs e)
    {
        try
        {
            ParentsChildren.Rows.Clear();
            Children.Rows.Clear();
            ds.Reset();
            //get Parent by FirstName and LastName
            using (DataSet dSetP = DataAccess.GetParentsByChild(ddlChildFullName.SelectedValue.Split()[0], ddlChildFullName.SelectedValue.Split()[1]))
            {
                DataTable dtParent = new DataTable();
                for (int t = 0; t < dSetP.Tables.Count; t++)
                {
                    dtParent = dSetP.Tables[t];
                    dtParent.TableName = "Parents";
                    ds.Tables.Add(dtParent.Copy());
                }
                lblParentsCount.Text = ds.Tables[0].Rows.Count.ToString();
                RedrawParentsTable();
            }

            //get specified Child
            using (DataSet dSetC = DataAccess.GetChild(ddlChildFullName.SelectedValue.Split()[0], ddlChildFullName.SelectedValue.Split()[1]))
            {
                DataTable dtChildren = new DataTable();
                for (int t = 0; t < dSetC.Tables.Count; t++)
                {
                    dtChildren = dSetC.Tables[t];
                }
                dtChildren.TableName = "Children";
                ds.Tables.Add(dtChildren.Copy());
                lblChildrenCount.Text = ds.Tables[1].Rows.Count.ToString();
                RedrawChildrenTable();
            }
        }
        catch (SqlException ex)
        {
            Label1.Text = SqlExceptionHelper.GetErrorExplanation(ex);
        }
    }

    private void DrawParentsTable()
    {
        int i = 0;
        foreach (DataRow dtRow in ds.Tables[0].Rows)
        {
            trow = new TableRow();
            tcellParentFN = new TableCell();
            tcellParentFN.Controls.Add(RenderTextBox("ParentFN" + i.ToString(), dtRow["ParentFN"].ToString()));
            tcellParentLN = new TableCell();
            tcellParentLN.Controls.Add(RenderTextBox("ParentLN" + i.ToString(), dtRow["ParentLN"].ToString()));
            tcellParentDoB = new TableCell();
            tcellParentDoB.Controls.Add(RenderTextBox("ParentDoB" + i.ToString(), dtRow["DateOfBirth"].ToString()));
            bcellUpdate = new TableCell();
            bcellUpdate.Controls.Add(RenderButton("UpdateParent" + i.ToString(), "Update"));
            bcellDelete = new TableCell();
            bcellDelete.Controls.Add(RenderButton("DeleteParent" + i.ToString(), "Delete"));
            trow.Cells.Add(tcellParentFN);
            trow.Cells.Add(tcellParentLN);
            trow.Cells.Add(tcellParentDoB);
            trow.Cells.Add(bcellUpdate);
            trow.Cells.Add(bcellDelete);
            ParentsChildren.Rows.Add(trow);
            i++;
        }
    }

    private void RedrawParentsTable()
    {
        int i = 0;
        foreach (DataRow dtRow in ds.Tables[0].Rows)
        {
            trow = new TableRow();
            tcellParentFN = new TableCell();
            tcellParentFN.Controls.Add(reRenderTextBox("ParentFN" + i.ToString(), dtRow["ParentFN"].ToString()));
            tcellParentLN = new TableCell();
            tcellParentLN.Controls.Add(reRenderTextBox("ParentLN" + i.ToString(), dtRow["ParentLN"].ToString()));
            tcellParentDoB = new TableCell();
            tcellParentDoB.Controls.Add(reRenderTextBox("ParentDoB" + i.ToString(), dtRow["DateOfBirth"].ToString()));
            bcellUpdate = new TableCell();
            bcellUpdate.Controls.Add(reRenderButton("UpdateParent" + i.ToString(), "Update"));
            bcellDelete = new TableCell();
            bcellDelete.Controls.Add(reRenderButton("DeleteParent" + i.ToString(), "Delete"));
            trow.Cells.Add(tcellParentFN);
            trow.Cells.Add(tcellParentLN);
            trow.Cells.Add(tcellParentDoB);
            trow.Cells.Add(bcellUpdate);
            trow.Cells.Add(bcellDelete);
            ParentsChildren.Rows.Add(trow);
            i++;
        }
    }


    private void DrawChildrenTable()
    {
        int i = 0;
        foreach (DataRow dtRow in ds.Tables[1].Rows)
        {
            trow = new TableRow();
            tcellChildFN = new TableCell();
            tcellChildFN.Controls.Add(RenderTextBox("ChildFN" + i.ToString(), dtRow["ChildFN"].ToString()));
            tcellChildLN = new TableCell();
            tcellChildLN.Controls.Add(RenderTextBox("ChildLN" + i.ToString(), dtRow["ChildLN"].ToString()));
            tcellChildDoB = new TableCell();
            tcellChildDoB.Controls.Add(RenderTextBox("ChildDoB" + i.ToString(), dtRow["DateOfBirth"].ToString()));
            bcellUpdate = new TableCell();
            bcellUpdate.Controls.Add(RenderButton("UpdateChild" + i.ToString(), "Update"));
            bcellDelete = new TableCell();
            bcellDelete.Controls.Add(RenderButton("DeleteChild" + i.ToString(), "Delete"));
            trow.Cells.Add(tcellChildFN);
            trow.Cells.Add(tcellChildLN);
            trow.Cells.Add(tcellChildDoB);
            trow.Cells.Add(bcellUpdate);
            trow.Cells.Add(bcellDelete);
            Children.Rows.Add(trow);
            i++;
        }
    }

    private void RedrawChildrenTable()
    {
        int i = 0;
        foreach (DataRow dtRow in ds.Tables[1].Rows)
        {
            trow = new TableRow();
            tcellChildFN = new TableCell();
            tcellChildFN.Controls.Add(reRenderTextBox("ChildFN" + i.ToString(), dtRow["ChildFN"].ToString()));
            tcellChildLN = new TableCell();
            tcellChildLN.Controls.Add(reRenderTextBox("ChildLN" + i.ToString(), dtRow["ChildLN"].ToString()));
            tcellChildDoB = new TableCell();
            tcellChildDoB.Controls.Add(reRenderTextBox("ChildDoB" + i.ToString(), dtRow["DateOfBirth"].ToString()));
            bcellUpdate = new TableCell();
            bcellUpdate.Controls.Add(reRenderButton("UpdateChild" + i.ToString(), "Update"));
            bcellDelete = new TableCell();
            bcellDelete.Controls.Add(reRenderButton("DeleteChild" + i.ToString(), "Delete"));
            trow.Cells.Add(tcellChildFN);
            trow.Cells.Add(tcellChildLN);
            trow.Cells.Add(tcellChildDoB);
            trow.Cells.Add(bcellUpdate);
            trow.Cells.Add(bcellDelete);
            Children.Rows.Add(trow);
            i++;
        }
    }

    private TextBox RenderTextBox(string suffix, string text)
    {
        TextBox tb = new TextBox();
        tb.ID = "txt" + suffix;
        tb.Text = text;
        tb.Width = 150;
        //tb.TextChanged += new EventHandler(textBox_TextChanged);
        return tb;
    }

    private TextBox reRenderTextBox(string suffix, string text)
    {
        TextBox tb = new TextBox();
        tb.ID = "txt" + suffix;
        tb.Text = text;
        tb.Width = 150;
        tb.Enabled = false;
        //tb.TextChanged += new EventHandler(textBox_TextChanged);
        return tb;
    }

    private Button RenderButton(string suffix, string text)
    {
        Button bt = new Button();
        bt.ID = "btn" + suffix;
        bt.Height = 20;
        bt.Width = 70;
        bt.Text = text;
        if (suffix.Substring(6, 6).Equals("Parent"))
        {
            if (text.Equals("Delete"))
                bt.Click += new EventHandler(btnDeleteParent_Click);
            else if (text.Equals("Update"))
                bt.Click += new EventHandler(btnUpdateParent_Click);
            return bt;
        }
        else
        {
            if (text.Equals("Delete"))
                bt.Click += new EventHandler(btnDeleteChild_Click);
            else if (text.Equals("Update"))
                bt.Click += new EventHandler(btnUpdateChild_Click);
            return bt;
        }
    }

    private Button reRenderButton(string suffix, string text)
    {
        Button bt = new Button();
        bt.ID = "btn" + suffix;
        bt.Height = 20;
        bt.Width = 70;
        bt.Text = text;
        bt.Enabled = false;
        if (suffix.Substring(6, 6).Equals("Parent"))
        {
            if (text.Equals("Delete"))
                bt.Click += new EventHandler(btnDeleteParent_Click);
            else if (text.Equals("Update"))
                bt.Click += new EventHandler(btnUpdateParent_Click);
            return bt;
        }
        else
        {
            if (text.Equals("Delete"))
                bt.Click += new EventHandler(btnDeleteChild_Click);
            else if (text.Equals("Update"))
                bt.Click += new EventHandler(btnUpdateChild_Click);
            return bt;
        }
    }

    protected void btnDeleteParent_Click(object sender, EventArgs e)
    {

    }

    protected void btnAddParent_Click(object sender, EventArgs e)
    {

    }

    protected void btnUpdateParent_Click(object sender, EventArgs e)
    {
        string btIndex = ((Button)sender).ID;
        string newParentFN, newParentLN, newParentDoB, oldParentFN, oldParentLN, oldParentDoB;

        string parentIndex = btIndex.Substring(15);
        TextBox tbParentFN = (TextBox)ParentsChildren.FindControl("txtParentFN" + parentIndex);
        TextBox tbParentLN = (TextBox)ParentsChildren.FindControl("txtParentLN" + parentIndex);
        TextBox tbParentDoB = (TextBox)ParentsChildren.FindControl("txtParentDoB" + parentIndex);
        newParentFN = tbParentFN.Text;
        newParentLN = tbParentLN.Text;
        newParentDoB = tbParentDoB.Text;

        oldParentFN = ds.Tables["Parents"].Rows[Convert.ToInt32(parentIndex)].Field<string>("ParentFN");
        oldParentLN = ds.Tables["Parents"].Rows[Convert.ToInt32(parentIndex)].Field<string>("ParentLN");
        //oldParentDoB = ds.Tables["Parents"].Rows[Convert.ToInt32(parentIndex)].Field<string>("ParentDoB");

        try
        {
            //update Parent
            DataAccess.UpdateParent(oldParentFN, oldParentLN, newParentFN, newParentLN, newParentDoB);
        }
        catch (SqlException ex)
        {
            Label1.Text = SqlExceptionHelper.GetErrorExplanation(ex);
        }
        
    }

    protected void btnUpdateChild_Click(object sender, EventArgs e)
    {

    }

    protected void btnDeleteChild_Click(object sender, EventArgs e)
    {

    }
}