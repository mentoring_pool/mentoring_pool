﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public partial class SelectFileToProcess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void upload_Click(object sender, EventArgs e)
    {
        if (fileread.HasFile)
        {
            try
            {
                if (fileread.PostedFile.ContentType == "text")
                {
                    if (fileread.PostedFile.ContentLength < 512000)
                    {
                        string filename = Path.GetFileName(fileread.FileName);
                    }
                }


                //process file
                Stream fileStream = fileread.PostedFile.InputStream;
                using (StreamReader fsr = new StreamReader(fileStream))
                {
                    ArrayList persons = new ArrayList();
                    ArrayList parentschildren = new ArrayList();
                    string line;
                    while ((line = fsr.ReadLine()) != null)
                    {
                        string[] tmpArray = line.Split(Convert.ToChar(","));
                        Person parent = new Person(tmpArray[3], tmpArray[4], tmpArray[5]);
                        persons.Add(parent);
                        Person child = new Person(tmpArray[0], tmpArray[1], tmpArray[2]);
                        persons.Add(child);
                        parentschildren.Add(new ParentsChildren(parent, child));
                    }

                    //Person[] arrayp = distincpersons.ToArray(typeof(Person)) as Person[];
                    ParentsChildrenSelect pcs = new ParentsChildrenSelect(persons, parentschildren);

                    IFormatter formatterSerialize = new BinaryFormatter();
                    Stream streamSerialize = new FileStream(Server.MapPath("ParentsChildrenFile.bin/ParentsChildrenFile.xml"), FileMode.Create, FileAccess.Write, FileShare.None);
                    formatterSerialize.Serialize(streamSerialize, pcs);
                    streamSerialize.Close();

                    //Session.Clear();
                    //Session["ParentsChildrenObject"] = pcs;
                    Response.Redirect("EditDataBeforSave.aspx");

                }
            }
            catch (FileNotFoundException ex)
            {
                Label1.Text = ExceptionHelper.GetFileNotFoundExplanation(ex);
            }
            catch (SerializationException ex)
            {
                Label1.Text = ExceptionHelper.GetSerializationExplanation(ex);
            }
            catch (IOException ex)
            {
                Label1.Text = ExceptionHelper.GetFileReadExplanation(ex);
            }
            catch (Exception ex)
            {
                // something else went wrong
            }
        }
    }
}