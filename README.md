TEST Parens - Children application.

Use TestCasesParentsChildren.doc to learn functionality and testing application.

ParentChildren.mdf:
Folder C:\SQL_certification\mentoring_pool\ParentChildrenV2.1\ParentChildren\App_Data contains ParentChildren.mdf and ParentChildren_log.ldf databases.
These are SQL Server databases. MDF - master database file where all the database information is stored. It contains the rows, columns, fields and data created by an application or user. Database column creation, modifications and information record creation, modification are all stored within this file for searching and application use. LDF - log database file. During various creation and modification process's within the MDF, all activity is logged in the LDF. As dynamic information is being processed in and out of memory, called transactions, data is stored in the LDF for error management. Consequently, all user activity is recorded for reference.
These databases can be attached to the SQL server after cloning from reporsitory.

ASPNETDB.mdf:
What is App_Data/ASPNETDB.MDF. The ASPNETDB.MDF is the default database for using the ASP.NET Application Services, which includes profiles, roles, membership and more. For example, in web.config you can use parameter roleManager. It will use aspnetdb.
If the ASPNETDB.mdf is not needed by your application, then you can simply remove it from the "App_Data" folder where this database file is probably located and also remove the line in the web.config file that references to it. 